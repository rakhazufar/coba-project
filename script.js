const prev = document.querySelector(".owl-prev");
prev.innerHTML = "<img src='./assets/Left-button.svg' alt=''>";

const next = document.querySelector(".owl-next");
next.innerHTML = "<img src='./assets/Right-button.svg' alt=''>";

window.addEventListener("scroll", () => {
  const navbar = document.querySelector(".navbar");
  if (window.scrollY >= 131) {
    navbar.classList.add("change-background");
  } else {
    navbar.classList.remove("change-background");
  }
});
